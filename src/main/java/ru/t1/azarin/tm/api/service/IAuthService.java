package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role[] roles);

}
