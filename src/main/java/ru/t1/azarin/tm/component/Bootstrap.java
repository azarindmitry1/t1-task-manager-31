package ru.t1.azarin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.azarin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.azarin.tm.api.repository.ICommandRepository;
import ru.t1.azarin.tm.api.repository.IProjectRepository;
import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.api.repository.IUserRepository;
import ru.t1.azarin.tm.api.service.*;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.dto.request.ServerAboutRequest;
import ru.t1.azarin.tm.dto.request.ServerVersionRequest;
import ru.t1.azarin.tm.endpoint.SystemEndpoint;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.azarin.tm.exception.system.CommandNotSupportedException;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.model.User;
import ru.t1.azarin.tm.repository.CommandRepository;
import ru.t1.azarin.tm.repository.ProjectRepository;
import ru.t1.azarin.tm.repository.TaskRepository;
import ru.t1.azarin.tm.repository.UserRepository;
import ru.t1.azarin.tm.service.*;
import ru.t1.azarin.tm.util.SystemUtil;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.azarin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final Server server = new Server(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(param);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
        return true;
    }

    public void processCommands(@Nullable final String command) {
        processCommands(command, true);
    }

    public void processCommands(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        final User testUser = userService.create("test_user", "test_user", Role.USUAL);

        projectService.add(admin.getId(), new Project("proj-one", "proj-one-desc"));
        projectService.add(admin.getId(), new Project("proj-two", "proj-two-desc"));
        projectService.add(admin.getId(), new Project("proj-three", "proj-three-desc"));

        taskService.add(admin.getId(), new Task("task-one", "task-desc-one"));
        taskService.add(admin.getId(), new Task("task-two", "task-desc-two"));
        taskService.add(admin.getId(), new Task("task-three", "task-desc-three"));
    }

    private void prepareStartup() {
        initPid();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        fileScanner.stop();
        server.stop();
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER THE COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommands(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[ERROR]");
            }
        }
    }

}