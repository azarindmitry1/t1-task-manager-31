package ru.t1.azarin.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ServerAboutResponse extends AbstractResponse {

    @Nullable
    private String name;

    @Nullable
    private String email;

}
