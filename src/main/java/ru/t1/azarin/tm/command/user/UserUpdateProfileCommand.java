package ru.t1.azarin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-update-profile";

    @NotNull
    public final static String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getUserService().updateUser(
                userId, firstName, middleName, lastName
        );
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
