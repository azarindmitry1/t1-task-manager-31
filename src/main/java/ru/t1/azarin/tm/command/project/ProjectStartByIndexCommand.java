package ru.t1.azarin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "project-start-by-index";

    @NotNull
    public final static String DESCRIPTION = "Start project by index.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        @NotNull final Integer index = TerminalUtil.nextInteger() - 1;
        @NotNull final String userId = getUserId();
        serviceLocator.getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
